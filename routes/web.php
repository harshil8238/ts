<?php
use Illuminate\Support\Facades\Auth;
// use DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('emp_login');
});
Route::get('/themetest', function () {
    return view('theme');
});
Route::get('/admin', function () {
    return view('login');
});
Route::post('/emp_login', 'Auth\LoginController@empLogin');

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard','DashboardController@index');
    Route::resources([
        'employees' => 'EmployeeController',
        'clients' => 'ClientController',
        'projects' => 'ProjectController',
        'tasks' => 'TaskController',
    ]);
    Route::get('/getClientList','ClientController@getClientList');
    Route::get('/getEmpList','EmployeeController@getEmpList');
    Route::get('/getProjectList','ProjectController@getProjectList');
    Route::get('/getTaskList/status/{status}','TaskController@getTaskList');

    Route::get('/getCompletedTasks','TaskController@getCompletedTasks');
    Route::post('/taskReportByDate', 'TaskController@getCompletedTaskByDate');
    Route::post('/empReportByYear', 'EmployeeController@getEmpReportByYear');

    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});
Route::group(['middleware' => ['auth:employees']], function () {
    Route::get('/empLogout', 'Auth\LoginController@logout');
    Route::get('/employee_view','EmployeeView\EmployeeController@index');
    Route::get('/getTaskListForEmployee','EmployeeView\EmployeeController@getTaskListForEmployee');
    Route::post('/setTaskStartTime','EmployeeView\EmployeeController@setTaskStartTime');
    Route::post('/setTaskEndTime','EmployeeView\EmployeeController@setTaskEndTime');
});