@section('css')
@stop

@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header row">
            <h1 class="col-md-8"> Task-Report: <span class="title-color"> {{ $date->format('dS F, Y') }} </span></h1>

            <form action="{{ asset('taskReportByDate') }}" method="POST">
                @csrf
                <div class="form-group col-md-4">
                    <div class="input-group date ">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" name="date" autocomplete="off" id="datepicker" placeholder="DD-MM-YYYY">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-info btn-flat">Go</button>
                        </span>
                    </div>
                </div>
            </form>

        </section>
        <section class="content">
                <div class="form-group col-md-12 alert alert-success" id="successMessage" style="display:none">
                    <span id="message"></span>
                </div>
            <table id="completed_task_list"  class="table table-bordered table-hover dashboard--table">
                <thead>
                <tr>
                    <th>Employee</th>
                    <th>Project</th>
                    <th>Task-ID</th>
                    <th>Task-Name</th>
                    <th>Working-Hour</th>
                </tr>
                </thead>
                <tbody>
                    @php
                        $existEmployeeId = null;
                        $existProjectId = null;
                    @endphp
                    @foreach ($completed_tasks as $value)
                    <tr>
                        @if (!$existEmployeeId || $existEmployeeId != $value->employee->id)
                        <td rowspan="{{ $emp_grp[$value->employee->id]->count() }}">{{ $value->employee->fullname}}</td>
                        @endif
                        @if (!$existProjectId || $existProjectId != $value->project->id)
                        <td rowspan="{{ $pro_grp[$value->employee->id][$value->project->id]->count() }}">{{ $value->project->project_name}}</td>
                        @endif
                        <td>{{ $value->id }} </td>
                        <td>{{ $value->task_name }} </td>
                        <td>{{ $value->working_hour }}</td>
                        @php
                                $existEmployeeId = $value->employee->id;
                                $existProjectId = $value->project->id;
                                @endphp
                        </tr>
                        @endforeach
                        @if (count($completed_tasks) == 0)
                            <tr class="text-red"><td colspan="5"> <center> No data available in Table </center></td><tr>
                        @endif
                </tbody>
            </table>
        </section>
    </div>
@stop
@section('js')

@stop
