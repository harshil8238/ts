@section('css')
{{-- <link rel="stylesheet" href="{{ asset('css/task.css') }}"> --}}
@stop

@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <section class="content-header row">
        <h1 class="col-md-11 title-color"> Tasks </h1>
        <div class="col-md-1">
            <button type="button" data-toggle="modal" data-target="#create_task_modal"
                class="btn btn-block btn-primary btn-flat"><i class="fa fa-fw fa-plus-square"></i> Add Task</button>
        </div>
    </section>
    <section class="content">
        <div class="form-group col-md-12 alert alert-success" id="successMessage" style="display:none">
            <span id="message"></span>
        </div>
        <table id="task_list" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Task-ID</th>
                    <th>Task-Name</th>
                    <th>Project</th>
                    <th>Employee</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>Task-Desc</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </section>
</div>
<div class="modal fade" id="create_task_modal" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Task Assign</h4>
        </div>
        <form id="task_form" action="{{ asset('tasks') }}" method="post">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Project</label>
                        <select class="form-control select2 select2-hidden-accessible" js-validate name="project_id"
                            style="width: 100%;" tabindex="-1" aria-hidden="true">
                            <option value="">SELECT</option>
                            @foreach ($projects as $value)
                            <option value="{{ $value->id}}">{{ $value->project_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Employee</label>
                        <select class="form-control select2 select2-hidden-accessible" js-validate name="employee_id"
                            style="width: 100%;" tabindex="-1" aria-hidden="true">
                            <option value="">SELECT</option>
                            @foreach ($employees as $value)
                            <option value="{{ $value->id}}">{{ $value->fullname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="task_name">Task-Name</label>
                        <input type="text" class="form-control" id="task_name" js-validate name="task_name"
                            placeholder="Enter Task-Name">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="task_desc">Task-Description</label>
                        <textarea name="task_desc" class="textarea task_desc" js-validate
                            placeholder="Place Task Description"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" id="addtask" class="btn btn-primary">Task Assign</button>
            </div>
        </form>
        </div>
    </div>
</div>
<div class="modal fade" id="show_task_modal" style="display: none;">
    <div class="modal-dialog modal-lg">
        <form id="show_task_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Task-Detail</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="task_id" value="">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="edit_project_id">Project-Name</label>
                            <select class="form-control select2 select2-hidden-accessible" setDisabled
                                id="edit_project_id" name="edit_project_id" style="width: 100%;" tabindex="-1"
                                aria-hidden="true">
                                <option>SELECT</option>
                                @foreach ($projects as $value)
                                <option value="{{ $value->id}}">{{ $value->project_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="edit_employee_id">Employee-Name</label>
                            <select class="form-control select2 select2-hidden-accessible" setDisabled
                                id="edit_employee_id" name="edit_employee_id" style="width: 100%;" tabindex="-1"
                                aria-hidden="true">
                                <option>SELECT</option>
                                @foreach ($employees as $value)
                                <option value="{{ $value->id}}">{{ $value->fullname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="edit_task_name">Task-Name</label>
                            <input type="text" class="form-control" setDisabled js-validate id="edit_task_name"
                                name="edit_task_name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="edit_task_desc">Task-Description</label>
                            <textarea id="edit_task_desc" name="edit_task_desc" setDisabled class="textarea task_desc"
                                js-validate placeholder="Place Task Description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" id="edit_task" class="btn btn-primary">Edit</button>
                    <button type="submit" id="update_task" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('js')
<script src="{{ asset('js/task.js') }}"></script>
@stop