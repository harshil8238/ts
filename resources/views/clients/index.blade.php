@section('css')
@stop

@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header row">
            <h1 class="col-md-11"> Clients </h1>
            <div class="col-md-1">
                <button type="button" data-toggle="modal" data-target="#create_client_modal" class="btn btn-block btn-primary btn-flat"><i class="fa fa-fw fa-plus-square"></i> Add Client</button>
            </div>
        </section>
        <section class="content">
                <div class="form-group col-md-12 alert alert-success" id="successMessage" style="display:none">
                    <span id="message"></span>
                </div>
            <table id="client_list"  class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Client-ID</th>
                    <th>Client-Name</th>
                    <th>Country</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </section>
    </div>
    <div class="modal fade" id="create_client_modal" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add - Client</h4>
            </div>
            <div class="modal-body">
                <form id="client_form" action="{{ asset('clients') }}" method="post">
                    @csrf
                    <div class="form-group col-md-12 alert alert-success" id="successMessage" style="display:none">
                        Client Add Succesfully.
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="client_name">Name</label>
                            <input type="text" class="form-control" id="client_name" js-validate  name="client_name" placeholder="Enter Client-Name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="client_country">Country</label>
                            <input type="text" class="form-control" id="client_country" js-validate  name="client_country" placeholder="Enter Client-Country">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" id="addClient" class="btn btn-primary">Add Client</button>
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit_client_modal" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Edit Client-Modal</h4>
                </div>
                <div class="modal-body">
                    <form id="edit_form" method="POST">
                        @csrf
                        <div class="row">
                            <input type="hidden" id="edit_client_id">
                            <div class="form-group col-md-6">
                                <label for="client_name">Name</label>
                                <input type="text" class="form-control" required id="edit_client_name" name="client_name" placeholder="Enter Client-Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="client_country">Country</label>
                                <input type="text" class="form-control" required id="edit_client_country" name="client_country" placeholder="Enter Client-Country">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update_client">Save changes</button>
                </div>
                </div>
            </div>
        </div>
@stop
@section('js')
    <script src="js/client.js"></script>
@stop
