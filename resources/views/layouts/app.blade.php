<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Task-System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  @yield('css')
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('theme/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('theme/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('theme/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('theme/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('theme/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/employee/employee.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')  }}">
  <style type="text/css">
      .red-border{ border-color: red}
      .hide_column {
          display : none;
      }
      .toolbar {
          float:left;
      }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <a href="{{ asset('theme/index2.html') }}" class="logo">
      <span class="logo-mini"><b>TS</b></span>
      <span class="logo-lg"><b>Task-Tracker</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <a href="{{ asset('logout') }}" class="btn btn-primary btn-lg">Logout</a>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="{{ asset('dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="">
          <a href="{{ asset('employees') }}">
            <i class="fa fa-users"></i> <span>Employee</span>
          </a>
        </li>
        <li class="">
            <a href="{{ asset('clients') }}">
              <i class="fa fa-user"></i> <span>Client</span>
            </a>
        </li>
        <li class="">
            <a href="{{ asset('projects') }}">
              <i class="fa fa-laptop"></i> <span>Project</span>
            </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i> <span>Task</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('tasks') }}"><i class="fa fa-list"></i> Task List</a></li>
            <li><a href="{{ asset('getCompletedTasks') }}"><i class="fa fa-list"></i> Task Report</a></li>
          </ul>
      </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->

        @yield('content')

  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; 2020 <a href="https://www.radicalloop.com/" target="_blank">radicalloop</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('theme/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('theme/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('theme/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
{{-- <script src="{{ asset('theme/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('theme/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('theme/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script> --}}
<script src="{{ asset('theme/bower_components/moment/min/moment.min.js') }}"></script>

<script src="{{ asset('theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ asset('theme/bower_components/chart.js/Chart.js') }}"></script>
{{-- <script src="{{ asset('theme/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script> --}}
<!-- FastClick -->
{{-- <script src="{{ asset('theme/bower_components/fastclick/lib/fastclick.js') }}"></script> --}}
<!-- AdminLTE App -->
<script src="{{ asset('theme/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('theme/dist/js/demo.js') }}"></script>
<script src=" {{ asset('theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src=" {{ asset('theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src=" {{ asset('js/validation.js') }}"></script>
@yield('js')
</body>
<script>
    $(function () {
      $('#datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        minDate: new Date("2019, 3,7"),
        maxDate: new Date()
      });

      $("#yearpicker").datepicker({
        autoclose: true,
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        changeMonth: true,
      changeYear: true,
      yearRange:  '1964:1991',
      defaultDate: '01-01-1964'
      });

    });
</script>
</html>
