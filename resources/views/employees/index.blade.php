@section('css')
@stop

@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header row">
            <h1 class="col-md-10"> Employee </h1>
            <div class="col-md-2">
                <button type="button" data-toggle="modal" data-target="#create_emp_modal" class="btn btn-block btn-primary btn-flat"><i class="fa fa-fw fa-plus-square"></i> Add-Employee</button>
            </div>
        </section>
        <section class="content">
                <div class="form-group col-md-12 alert alert-success" id="successMessage" style="display:none">
                    <span id="message"></span>
                </div>
            <table id="emp_list" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Employee-ID</th>
                    <th>FullName</th>
                    <th>DOJ</th>
                    <th>E-Mail</th>
                    <th>On-Working-Project</th>
                    <th>On-Working-Task</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </section>
    </div>
    <div class="modal fade" id="create_emp_modal" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add - Employee</h4>
                </div>
                <div class="modal-body">
                    <form id="employee_form" action="{{ asset('employees') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="emp_fullname">Full-Name</label>
                                <input type="text" class="form-control" id="emp_fullname" js-validate name="emp_fullname" placeholder="Enter FullName">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="emp_doj">DOJ</label>
                                <input type="text" class="form-control" id="datepicker" js-validate name="emp_doj" placeholder="DD-MM-YYYY">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                    <label for="emp_email">E-Mail</label>
                                    <input type="email" class="form-control" id="emp_email" js-validate name="emp_email" placeholder="Enter email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="emp_password">Password</label>
                                <input type="password" class="form-control" id="emp_password" js-validate name="emp_password" placeholder="Password">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" id="addEmployee" class="btn btn-primary">Add Employee</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit_emp_modal" style="display: none;">
            <div class="modal-dialog modal-lg">
            <form id="edit_employee_form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Edit-Emloyee-Detail</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="edit_emp_id" value="">
                        <div class="form-group">
                            <label for="emp_fullname">Full-Name</label>
                            <input type="text" class="form-control" id="edit_emp_fullname" value="" name="emp_fullname" placeholder="Enter FullName">
                        </div>
                        <div class="form-group">
                            <label for="emp_email">E-Mail</label>
                            <input type="email" class="form-control" id="edit_emp_email" value="" name="emp_email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" id="update_emp" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
@stop
@section('js')
    <script src="js/employee.js"></script>
@stop
