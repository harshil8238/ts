@section('css')
@stop

@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <section class="content-header row">
        <h1 class="col-md-4"> Performance - Year :
            <span class="title-color"> {{ $year }} </span>
        </h1>
        <h1 class="col-md-4">
            <span class="title-color"> Employee : {{ ucfirst($employee->fullname) }} </span>
        </h1>
        <form action="{{ asset('empReportByYear') }}" method="POST">
            @csrf
            <div class="form-group col-md-4">
                <div class="input-group date ">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="year" autocomplete="off" id="yearpicker" placeholder="Year">
                    <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go</button>
                    </span>
                </div>
            </div>
        </form>
    </section>
    <section class="content row">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Performance Chart</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="barChart" style="height: 230px; width: 754px;" width="754" height="230"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Employee Task</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="completed_task_list"  class="table table-bordered table-hover dashboard--table">
                            <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Project</th>
                                <th>Task-ID</th>
                                <th>Task-Name</th>
                                <th>Working-Hour</th>
                                <th>Task-Create Date</th>
                                <th>Start-Date</th>
                                <th>End-Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php
                                    $existEmployeeId = null;
                                    $existProjectId = null;
                                @endphp
                                @foreach ($completed_tasks as $value)
                                <tr>
                                    @if (!$existEmployeeId || $existEmployeeId != $value->employee->id)
                                    <td rowspan="{{ $emp_grp[$value->employee->id]->count() }}">{{ $value->employee->fullname}}</td>
                                    @endif
                                    @if (!$existProjectId || $existProjectId != $value->project->id)
                                    <td rowspan="{{ $pro_grp[$value->employee->id][$value->project->id]->count() }}">{{ $value->project->project_name}}</td>
                                    @endif
                                    <td>{{ $value->id }} </td>
                                    <td>{{ $value->task_name }} </td>
                                    <td>{{ ($value->working_hour) ?: '-' }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>{{ ($value->start_time) ?: '-' }}</td>
                                    <td>{{ ($value->end_time) ?: '-' }}</td>
                                    <td>
                                        @if (!$value->start_time)
                                            <span class="label label-danger">OPEN</span>
                                        @elseif($value->start_time && $value->end_time)
                                            <span class="label label-success">COMPLETED</span>
                                        @else
                                            <span class="label label-warning">IN PROGRESS</span>
                                        @endif
                                    </td>
                                    @php
                                        $existEmployeeId = $value->employee->id;
                                        $existProjectId = $value->project->id;
                                    @endphp
                                </tr>
                                @endforeach
                                    @if (count($completed_tasks) == 0)
                                        <tr class="text-red"><td colspan="9"> <center> No data available in Table </center></td><tr>
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop
@section('js')
    <script src="/js/employee.js"></script>
    <script>
        var total_task = [];
        var completed_task = [];
        var task = {!! $empTaskDetail !!};

        for (var i=1;i<13;i++) {
            if (task[i] && task[i].month == i) {
                total_task.push(task[i].totaltask_count);
                completed_task.push(task[i].completedtask_count);
            }
            else {
                total_task.push(0);
                completed_task.push(0);
            }
        }

  $(function () {
    var areaChartData = {
        labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Desember'],
        datasets: [
          {
            label               : 'Electronics',
            fillColor           : 'rgba(210, 214, 222, 1)',
            strokeColor         : 'rgba(210, 214, 222, 1)',
            pointColor          : 'rgba(210, 214, 222, 1)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : total_task
          },
          {
            label               : 'Digital Goods',
            fillColor           : 'rgba(60,141,188,0.9)',
            strokeColor         : 'rgba(60,141,188,0.8)',
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : completed_task
          }
        ]
      }

        var barChartCanvas                   = $('#barChart').get(0).getContext('2d');
        var barChart                         = new Chart(barChartCanvas);
        var barChartData                     = areaChartData;
        barChartData.datasets[1].fillColor   = '#00a65a';
        barChartData.datasets[1].strokeColor = '#00a65a';
        barChartData.datasets[1].pointColor  = '#00a65a';
        var barChartOptions                  = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero        : true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines      : true,
          //String - Colour of the grid lines
          scaleGridLineColor      : 'rgba(0,0,0,.05)',
          //Number - Width of the grid lines
          scaleGridLineWidth      : 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines  : true,
          //Boolean - If there is a stroke on each bar
          barShowStroke           : true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth          : 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing         : 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing       : 1,
          //String - A legend template
          legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
          //Boolean - whether to make the chart responsive
          responsive              : true,
          maintainAspectRatio     : true
        }

        barChartOptions.datasetFill = false;
        barChart.Bar(barChartData, barChartOptions);
      });
    </script>
@stop
