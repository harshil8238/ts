@section('css')
@stop

@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header row">
            <h1 class="col-md-11"> Projects </h1>
            <div class="col-md-1">
                <button type="button" data-toggle="modal" data-target="#create_project_modal" class=" btn btn-block btn-primary btn-flat"><i class="fa fa-fw fa-plus-square"></i> Add Project</button>
            </div>
        </section>
        <section class="content">
                <div class="form-group col-md-12 alert alert-success" id="successMessage" style="display:none">
                    <span id="message"></span>
                </div>
            <table id="project_list"  class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Project-ID</th>
                    <th>Project-Name</th>
                    <th>Client</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </section>
    </div>
    <div class="modal fade" id="create_project_modal" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add - Project</h4>
                </div>
                <div class="modal-body">
                    <form id="project_form" action="{{ asset('projects') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Client</label>
                                <select class="form-control select2 select2-hidden-accessible" js-validate name="client_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option value="">SELECT</option>
                                    @foreach ($clients as $value)
                                        <option value="{{ $value->id}}">{{ $value->client_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_name">Project-Name</label>
                                <input type="text" class="form-control" id="project_name" js-validate name="project_name" placeholder="Enter Project-Name">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" id="addproject" class="btn btn-primary">Add project</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit_project_modal" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Edit-Project-Detail</h4>
                    </div>
                    <div class="modal-body">
                        <form id="edit_project_form">
                            <input type="hidden" id="edit_project_id" value="">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Client</label>
                                    <select class="form-control select2 select2-hidden-accessible" id="edit_client_id" name="client_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            <option>SELECT</option>
                                        @foreach ($clients as $value)
                                            <option value="{{ $value->id}}">{{ $value->client_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="project_name">Project-Name</label>
                                    <input type="text" class="form-control" id="edit_project_name" value="" name="project_name" placeholder="Enter Name">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="update_project">Update</button>
                    </div>
                </div>
            </div>
        </div>
@stop
@section('js')
    <script src="js/project.js"></script>
@stop
