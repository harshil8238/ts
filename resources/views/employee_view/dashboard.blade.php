@extends('layouts.emp_view')
@section('content')
  <div class="content-wrapper dashboard-view">
    <section class="content-header">
      <h1 class="title-color">
        Dashboard
      </h1>
    </section>
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL TASK</span>
              <span class="info-box-number">{{ $totalTask }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-tasks"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PENDING TASK</span>
              <span class="info-box-number" id="pendingTask">{{ $pendingTask }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-tasks"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">IN PROGRESS TASK</span>
              <span class="info-box-number" id="inProgressTask">{{ $inProgressTask }}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-tasks"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">COMPLETED TASK</span>
              <span class="info-box-number" id="completedTask">{{ $completedTask }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-md-12">
          <table id="emp_task_list"  class="table table-bordered table-hover dashboard--table">
              <thead>
              <tr>
                  <th>Task-ID</th>
                  <th>Task-Name</th>
                  <th>Project</th>
                  <th>Action</th>
                  <th>Working Hour</th>
                  <th>Task-Desc</th>
              </tr>
              </thead>
              <tbody></tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
  <div class="modal fade" id="show_emp_task_modal" style="display: none;">
      <div class="modal-dialog modal-lg">
      <form id="show_emp_task_form">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                  <h4 class="modal-title">Task-Detail</h4>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="form-group col-md-12">
                          <label for="project_name">Project-Name</label>
                          <input type="text" class="form-control" disabled id="project_name">
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-md-12">
                          <label for="task_name">Task-Name</label>
                          <input type="text" class="form-control" disabled id="task_name">
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-md-12">
                          <label for="task_desc">Task-Description</label>
                          <textarea id="task_desc" disabled class="textarea task_desc"></textarea>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
              </div>
          </div>
      </form>
      </div>
  </div>
@stop

  @section('js')
  <script src="{{ asset('js/emp_view/emp_dashboard.js') }}"></script>
  <script>
    var emp_id = {{ $employee->id }}
  </script>
  @stop