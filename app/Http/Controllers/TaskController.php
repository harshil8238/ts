<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Employee;
use App\Project;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tasks.index',['employees' => Employee::all(), 'projects' => Project::all()]);
    }

    public function getTaskList(Request $request, $status) {
        $tasks = Task::select('tasks.*','projects.project_name','employees.fullname')
                        ->join('projects', 'tasks.project_id', '=', 'projects.id')
                        ->join('employees', 'tasks.employee_id', '=', 'employees.id')
                        ->whereNull('tasks.deleted_at');
                        if ($status == 'open'){
                            $tasks->whereNull('tasks.start_time');
                        }
                        if ($status == 'inprogress'){
                            $tasks->whereNotNull('tasks.start_time');
                            $tasks->whereNull('tasks.working_hour');
                        }
                        if ($status == 'completed'){
                            $tasks->whereNotNull('tasks.working_hour');
                        }
                        $tasks->withTrashed()->get();

        return Datatables::of($tasks)
                        ->addColumn('status', function ($tasks) {
                            if (!$tasks->start_time) { return 'Open'; }
                            elseif ($tasks->start_time && !$tasks->end_time) { return 'In Progress'; }
                            else { return 'Completed'; }
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task;
        $task->project_id = $request->project_id;
        $task->employee_id = $request->employee_id;
        $task->task_name = $request->task_name;
        $task->task_desc = $request->task_desc;
        $task->save();
        return ['success' => 'Add Task Successful'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task->project_id = $request->edit_project_id;
        $task->employee_id = $request->edit_employee_id;
        $task->task_name = $request->edit_task_name;
        $task->task_desc = $request->edit_task_desc;
        $task->save();
        return response()->json(['success' => 'Task Update Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return ['success' => 'Task Delete Successfully'];
    }

    public function getCompletedTasks () {
        return view('tasks.task_reaport', $this->getTaskReport(Carbon::now()));
    }

    public function getCompletedTaskByDate (Request $request) {
        return view('tasks.task_reaport', $this->getTaskReport(Carbon::createFromFormat('d/m/Y', $request->date)));
    }

    public function getTaskReport($date) {
        $completed_tasks = Task::with('employee', 'project')
                                ->whereNotNull('working_hour')
                                ->whereDate('created_at', $date)
                                ->orderBy('employee_id')
                                ->orderBy('project_id')
                                ->orderBy('id')
                                ->get();

        $pro_grp = $completed_tasks->groupBy(['employee_id',
            function ($item) { return $item['project_id'];},
        ], true);

        $emp_grp = $completed_tasks->groupBy('employee_id');
        return compact('completed_tasks', 'emp_grp', 'pro_grp', 'date');
    }
}
