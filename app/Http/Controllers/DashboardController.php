<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Employee;
use App\Project;
use App\Task;

class DashboardController extends Controller
{
    function index () {
        $clientCount = Client::all()->count();
        $taskCount = Task::all()->count();
        $projectCount = Project::all()->count();
        $employeeCount = Employee::all()->count();
        return view('dashboard', compact('clientCount', 'taskCount', 'projectCount', 'employeeCount'));
    }
}
