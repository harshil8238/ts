<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.index');
    }

    public function getEmpList() {
        $employees = Employee::with('task')->get();
        return Datatables::of($employees)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $employee = new Employee;
        $employee->fullname = $request->emp_fullname;
        $employee->doj = Carbon::createFromFormat('d/m/Y', $request->emp_doj);
        $employee->email = $request->emp_email;
        $employee->password = bcrypt($request->emp_password);
        $employee->save();
        return ['success' => 'Employee Add Successfully'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $year = Carbon::now()->year;
        $data = $this->getEmpPerformance($employee, $year);

        $completed_tasks = $data['completed_tasks'];
        $empTaskDetail = $data['empTaskDetail'];
        $pro_grp = $data['pro_grp'];
        $emp_grp = $data['emp_grp'];

        return view('employees.emp_detail', compact('employee', 'completed_tasks', 'year', 'emp_grp', 'pro_grp', 'empTaskDetail'));
    }

    public function getEmpReportByYear(Request $request) {
        $employee = Employee::find($request->employee_id);
        $year = $request->year;
        $data = $this->getEmpPerformance($employee, $year);

        $completed_tasks = $data['completed_tasks'];
        $empTaskDetail = $data['empTaskDetail'];
        $pro_grp = $data['pro_grp'];
        $emp_grp = $data['emp_grp'];

        return view('employees.emp_detail', compact('employee', 'completed_tasks', 'year', 'emp_grp', 'pro_grp', 'empTaskDetail'));
    }

    public function getEmpPerformance($employee, $year) {
        $completed_tasks = Task::with('employee', 'project')
                        ->where('employee_id', $employee->id)
                        ->whereYear('created_at', $year)
                        ->orderBy('employee_id')
                        ->orderBy('project_id')
                        ->orderBy('id')
                        ->get();

        $pro_grp = $completed_tasks->groupBy(['employee_id',
        function ($item) { return $item['project_id'];},
        ], true);

        $emp_grp = $completed_tasks->groupBy('employee_id');

        $empTaskDetail =  DB::table('tasks')
                        ->select(DB::raw('count(*) as totaltask_count, count(working_hour) as completedtask_count, MONTH(created_at) as month'))
                        ->where('employee_id', $employee->id)
                        ->whereYear('created_at', $year)
                        ->groupBy(DB::raw('MONTH(created_at)'))
                        ->get()->keyBy('month');
        return compact('completed_tasks', 'empTaskDetail', 'pro_grp', 'emp_grp');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        dd("Edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->fullname = $request->emp_fullname;
        $employee->email = $request->emp_email;
        $employee->save();
        return ['message' => 'Employee Data Update Successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json(['success' => 'Employee Delete Successfully']);
    }
}
