<?php

namespace App\Http\Controllers\EmployeeView;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use App\Employee;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Auth::guard('employees')->user();
        $totalTask = Task::where('employee_id', $employee->id)->get()->count();
        $pendingTask = Task::where('employee_id', $employee->id)
                            ->where('start_time', '=', null)
                            ->get()
                            ->count();
        $inProgressTask = Task::where('employee_id', $employee->id)
                            ->where('start_time', '!=', null)
                            ->where('end_time', '=', null)
                            ->get()
                            ->count();
        $completedTask = Task::where('employee_id', $employee->id)
                            ->where('start_time', '!=', null)
                            ->where('end_time', '!=', null)
                            ->get()
                            ->count();
        return view('employee_view/dashboard', ['employee' => $employee, 'totalTask' => $totalTask, 'pendingTask' => $pendingTask, 'inProgressTask' => $inProgressTask, 'completedTask' => $completedTask]);
    }

    public function getTaskListForEmployee(Request $request) {
        $tasks = Task::select('tasks.id','tasks.task_name','projects.project_name','tasks.task_desc','tasks.start_time','tasks.end_time', 'tasks.working_hour')
                        ->join('projects', 'tasks.project_id', '=', 'projects.id')
                        ->where('tasks.deleted_at',null)
                        ->where('tasks.employee_id',$request->get('id'))
                        ->withTrashed()
                        ->get();
        return Datatables::of($tasks)->make(true);
    }

    public function setTaskStartTime(Request $request) {
        $emp_task = Task::find($request->id);
        $emp_task->start_time = Carbon::now();
        $emp_task->save();
        return ['message' => 'Task Start'];
    }

    public function setTaskEndTime(Request $request) {
        $emp_task = Task::find($request->id);
        $from = carbon::parse($emp_task->start_time);
        $to = Carbon::now();
        $diff_in_hour = $to->diff($from)->format('%H:%I');

        $emp_task->end_time = $to;
        $emp_task->working_hour = $diff_in_hour;
        $emp_task->save();

        return ['message' => 'Task End', 'working_hour' => $diff_in_hour];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
