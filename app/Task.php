<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    public function project() {
        return $this->belongsTo('App\Project');
    }

    public function employee() {
        return $this->belongsTo('App\Employee');
    }
}
