$(document).ready(function(e){
    var working_project = '';
    var working_task = '';
    var table = $('#emp_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getEmpList',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'fullname', name: 'fullname'},
            {data: 'doj', name: 'doj',
                render: function(data){
                    return moment(data).format("Do MMMM, YYYY");
                }
            },
            {data: 'email', name: 'email'},
            {data: null, name: null, orderable: false, searchable: false},
            {data: null, name: null, orderable: false, searchable: false},
            {data: null, name: null, orderable: false, searchable: false}
        ],
        rowCallback: function( row, data ) {
            working_project = '<span class="label label-warning">Available</span>';
            working_task = '<span class="label label-warning">Available</span>';
            $.each(data.task, function (i, v) {
                if (v.start_time && !(v.end_time)) {
                    working_project = '<span class="label label-success">' + v['project'].project_name + '</span>';
                    working_task = '<span class="label label-success">' + v.task_name + '</span>';
                }
            });
            var action = '<a id="'+ data.id +'" data-emp_name="' + data.fullname + '" class="edit_emp btn btn-primary"> <i class="fa fa-edit"></i> </a> '+
                            '<a id="'+ data.id +'" class="delete_emp btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $('td:eq(6)', row).html(action);
            $('td:eq(5)', row).html(working_task);
            $('td:eq(4)', row).html(working_project);
            $('td:eq(1)', row).html('<a id="'+ data.id +'" class="show_emp_detail" href="/employees/'+ data.id +'">'+ data.fullname +'</a>');
        }
    });

    $('#addEmployee').on('click', function (e) {
        e.preventDefault();
        var error  = validate_form('employee_form');
        if (error) {
            return false;
        }
        var emp_form = $('#employee_form');

        $.ajax({
            url: emp_form.attr('action'),
            data:emp_form.serialize(),
            type:'post',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.success);
                emp_form[0].reset();
                table.ajax.reload();
                $('#create_emp_modal').modal('toggle');
            }
        });
    });

    $(document).on('click', '.edit_emp', function (e) {
        e.preventDefault();

        var obj = $(this);
        var row = obj.closest('tr');
        $('#edit_emp_id').val(obj.attr('id'));
        $('#edit_emp_fullname').val(obj.data('emp_name'));
        $('#edit_emp_email').val(row.find('td:eq(3)').html());
        $('#edit_emp_modal').modal('show');

    });

    $('#update_emp').on('click', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/employees/'+$('#edit_emp_id').val(),
            method:'PUT',
            dataType: 'json',
            data:$('#edit_employee_form').serialize(),
            success: function (result) {
                $('#edit_emp_modal').modal('toggle');
                $('#successMessage').show();
                $('#message').html(result.message);
                table.ajax.reload();
            },
        });
    });

    $(document).on('click', '.delete_emp', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/employees/'+$(this).attr('id'),
            method:'DELETE',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.success);
                table.ajax.reload();
            }
        });
    });

});