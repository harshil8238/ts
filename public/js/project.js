$(document).ready(function(e){
    var table = $('#project_list').DataTable({
        processing: true,
        serverside: true,
        ajax: 'getProjectList',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'project_name', name: 'project_name'},
            {data: null, name: null},
            {data: null, name: null, orderable: false, searchable: false}
        ],
        rowCallback: function( row, data ) {
            var client = '<td>'+ data.client.client_name +'</td>'
            var action = '<a id="'+ data.id +'" class="edit_project btn btn-primary" data-client_id="'+ data.client.id +'"> <i class="fa fa-edit"></i> </a> '+
                         '<a id="'+ data.id +'" class="delete_project btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $('td:eq(3)', row).html(action);
            $('td:eq(2)', row).html(client);
        }
    });

    $('#addproject').on('click', function (e) {
        e.preventDefault();
        var error  = validate_form('project_form');
        if (error) {
            return false;
        }

        var project_form = $('#project_form');

        $.ajax({
            url: project_form.attr('action'),
            data:project_form.serialize(),
            type:'post',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.success);
                project_form[0].reset();
                table.ajax.reload();
                $('#create_project_modal').modal('toggle');
            }
        });
    });

    $(document).on('click', '.edit_project', function (e) {
        e.preventDefault();
        var obj = $(this);
        console.log($(obj).data('client_id'));
        var row = obj.closest('tr');
        $('#edit_project_id').val(obj.attr('id'));
        $('#edit_project_name').val(row.find('td:eq(1)').html());
        $('#edit_client_id').val($(obj).data('client_id'));
        $('#edit_project_modal').modal('show');
    });

    $('#update_project').on('click', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/projects/'+$('#edit_project_id').val(),
            method:'PUT',
            dataType: 'json',
            data:$('#edit_project_form').serialize(),
            success: function (result) {
                $('#edit_project_modal').modal('toggle');
                $('#successMessage').show();
                $('#message').html(result.success);
                table.ajax.reload();
            },
        });
    });

    $(document).on('click', '.delete_project', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/projects/'+$(this).attr('id'),
            method:'DELETE',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.message);
                table.ajax.reload();
            }
        });
    });

});