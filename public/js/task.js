$(document).ready(function(e){
    var table = $('#task_list').DataTable({
        processing: true,
        serverside: true,
        ajax: 'getTaskList/status/all',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'task_name', name: 'task_name'},
            {data: 'project_name', name: 'project_name'},
            {data: 'fullname', name: 'fullname'},
            {data: 'status', name: 'status'},
            {data: null, name: null, orderable: false, searchable: false},
            {data: 'task_desc', name: 'task_desc', className: 'hide_column'}
        ],
        initComplete: function(){
            var filter_status = '<div class="col-sm-4 ml-neg-84">'+
                                    '<select id="filter_task_status" name="filter_status" class="form-control input-sm">'+
                                        '<option value="all"> All </option>'+
                                        '<option value="open"> Open </option>'+
                                        '<option value="inprogress"> In Progress </option>'+
                                        '<option value="completed"> Completed </option>'+
                                    '</select>'+
                                '</div>';
            $('#task_list_length').parent('div').attr('class', 'col-sm-2')
            $('#task_list_filter').parent('div').addClass('ml-pos-84')

            $('#task_list_wrapper').children('div:eq(0)').find('div:eq(0)').after(filter_status);
        },
        rowCallback: function( row, data ) {
            var status = '';
            if (data.status == 'Open') {
                status +='<span class="label label-danger">OPEN</span>';
            } else if (data.status == 'Completed') {
                status +='<span class="label label-success">COMPLETED</span>';
            } else {
                status +='<span class="label label-warning">IN PROGRESS</span>';
            }
            var action = '<a id="'+ data.id +'" class="show_task btn btn-primary" data-project_id="'+ data.project_id +'" data-employee_id="'+ data.employee_id +'"> <i class="fa fa-edit"></i> </a> '+
                         '<a id="'+ data.id +'" class="delete_task btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $('td:eq(5)', row).html(action);
            $('td:eq(4)', row).html(status);
        }
    });

    $('#addtask').on('click', function (e) {
        e.preventDefault();
        var error  = validate_form('task_form');
        if (error) {
            return false;
        }
        var task_form = $('#task_form');

        $.ajax({
            url: task_form.attr('action'),
            data:task_form.serialize(),
            type:'post',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.success);
                task_form[0].reset();
                $('#create_task_modal').modal('toggle');
                table.ajax.reload();
            }
        });
    });

    $(document).on('click', '.show_task', function () {
        var obj = $(this);
        var row = obj.closest('tr');
        var disabledEle = $('#show_task_form :input[setDisabled]');
        $.each(disabledEle, function (i, v) {
            $(v).attr("disabled", true);
        });
        $('#edit_task').show();
        $('#update_task').hide();
        $('#task_id').val($(this).attr('id'));
        $('#edit_project_id').val($(this).data('project_id'));
        $('#edit_employee_id').val($(this).data('employee_id'));
        $('#edit_task_name').val(row.find('td:eq(1)').html());
        $('#edit_task_desc').val(row.children('td:eq(6)').html());
        $('#show_task_modal').modal('toggle');
    });

    $('#edit_task').on('click', function (e) {
        e.preventDefault();
        var disabledEle = $('#show_task_form :input[setDisabled]');
        $.each(disabledEle, function (i, v) {
            $(v).removeAttr("disabled");
        });
        $('#edit_task').hide();
        $('#update_task').show();
    });

    $(document).on('click', '#update_task', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/tasks/'+$('#task_id').val(),
            method:'PUT',
            dataType: 'json',
            data:$('#show_task_form').serialize(),
            success: function (result) {
                $('#show_task_modal').modal('toggle');
                $('#successMessage').show();
                $('#message').html(result.success);
                table.ajax.reload();
            },
        });
    });

    $(document).on('click', '.delete_task', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/tasks/'+$(this).attr('id'),
            method:'DELETE',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.success);
                table.ajax.reload();
            }
        });
    });

    $(document).on('change', '#filter_task_status', function (e) {
        table.ajax.url('getTaskList/status/'+$(this).val()).load();
    });

});