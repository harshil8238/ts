$(document).ready(function(e){
    var running = false;
    var pending_task =  $('#pendingTask');
    var in_progress_task = $('#inProgressTask');
    var completed_task = $('#completedTask');

    var table = $('#emp_task_list').DataTable({
        processing: true,
        serverside: true,
        ajax: {
            'url': 'getTaskListForEmployee',
            'type': 'get',
            'data': function (d) {
                d.id = emp_id;
            },
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'task_name', name: 'task_name'},
            {data: 'project_name', name: 'project_name'},
            {data: null, name: null},
            {data: null, name: null},
            {data: 'task_desc', name: 'task_desc', className: 'hide_column'}
        ],
        rowCallback: function( row, data ) {
            var action = '<a class="show_emp_task btn btn-primary" data-toggle="tooltip" title="View"> <i class="fa fa-eye"></i> </a> ';
            var working_hour = (data.working_hour) ? data.working_hour : 0 ;

            if (!data.start_time) {
                action +='<a id="'+data.id+'" class="task_start_time btn btn-danger" data-toggle="tooltip" title="START TASK"> <i class="fa fa-clock-o"></i></a>';
            } else if (data.start_time && !data.end_time) {
                action +='<a id="'+data.id+'" class="task_end_time btn btn-warning data-toggle="tooltip" title="END TASK"> END TASK </a>';
                running = true;
            } else {
                action +='<a id="'+data.id+'" class="btn btn-success" data-toggle="tooltip" title="TASK COMPLETED"> <i class="glyphicon glyphicon-ok-sign"></i> </a>';
            }
            $('td:eq(3)', row).html(action);
            $('td:eq(4)', row).html('<span>'+ working_hour +'</span>');
        },
        initComplete: function(settings, json) {
            if (running) {
                $('.task_start_time').attr('disabled', true);
            }
        }
    });

    $(document).on('click', '.show_emp_task', function(e) {
        e.preventDefault();
        var row = $(this).closest('tr');
        $('#project_name').val(row.find('td:eq(1)').html());
        $('#task_name').val(row.find('td:eq(2)').html());
        $('#task_desc').val(row.children('td:eq(5)').html());
        $('#show_emp_task_modal').modal('toggle');
    })

    $(document).on('click', '.task_start_time', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        if (running) {
            alert("Previous task was running, unable to start new task");
            return false;
        }

        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/setTaskStartTime',
            method:'post',
            data: {id},
            success: function (result) {
                pending_task.html(parseInt(pending_task.html()) - 1);
                in_progress_task.html(parseInt(in_progress_task.html()) + 1);
                $('#'+id).closest('td').append(' <a id="'+ id +'" class="task_end_time btn btn-warning">END TASK</a>');
                $('#'+id).remove();
                $('.task_start_time').attr('disabled', true);
                running = true;
            },
        });

    });

    $(document).on('click', '.task_end_time', function () {
        var id = $(this).attr('id');

        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/setTaskEndTime',
            method:'post',
            data: {id},
            success: function (result) {
                in_progress_task.html(parseInt(in_progress_task.html()) - 1);
                completed_task.html(parseInt(completed_task.html()) + 1);
                $('#'+id).parents('tr').find('span').html(result.working_hour);
                $('#'+id).closest('td').append('<a id="'+id+'" class="btn btn-success"> <i class="glyphicon glyphicon-ok-sign"></i> </a>');
                $('#'+id).remove();
                $('.task_start_time').attr('disabled', false);
                running = false;
            },
        });
    });

});