
function validate_form(form) {
    var form = $('#'+form+' :input[js-validate]');
    var error = false;
    $.each(form, function (i,inp) {
        if ($(inp).val() == '') {
            error = errorShow(inp);
        }
        if ($(inp).attr('type') == 'email' && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(inp).val())) {
            error = errorShow(inp);
        }
        if (($(inp).attr('type') == 'text') && /\d/.test($(inp).val())) {
            if ($(this).attr('id') != 'datepicker') {
                error = errorShow(inp);
            }
        }
    });
    return error;
}

function errorShow(inp) {
    $(inp).addClass('red-border');
    return true;
}