$(document).ready(function(e){

    var table = $('#client_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getClientList',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'client_name', name: 'client_name'},
            {data: 'client_country', name: 'client_country'},
            {data: null, name: null, orderable: false, searchable: false},
        ],
        rowCallback: function( row, data ) {
            var action = '<a id="'+ data.id +'" class="edit_client btn btn-primary"> <i class="fa fa-edit"></i> </a> '+
                         '<a id="'+ data.id +'" class="delete_client btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $('td:eq(3)', row).html(action);
        }
    });

    $('#addClient').on('click', function(e) {
        e.preventDefault();
        var error  = validate_form('client_form');
        if (error) {
            return false;
        }
        var client_form = $('#client_form');

        $.ajax({
            url: client_form.attr('action'),
            data:client_form.serialize(),
            type:'post',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.success);
                client_form[0].reset();
                table.ajax.reload();
                $('#create_client_modal').modal('toggle');
            }
        });
    });

    $(document).on("click", ".delete_client", function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/clients/'+$(this).attr('id'),
            method:'DELETE',
            dataType: 'json',
            success: function (result) {
                $('#successMessage').show();
                $('#message').html(result.message);
                table.ajax.reload();
            }
        });
    });

    $(document).on("click", ".edit_client", function(e) {
        e.preventDefault();
        var obj = $(this);
        var row = obj.closest('tr');

        $('#edit_client_id').val(obj.attr('id'));
        $('#edit_client_name').val(row.find('td:eq(1)').html());
        $('#edit_client_country').val(row.find('td:eq(2)').html());
        $('#edit_client_modal').modal('show');
    });

    $(document).on("click", "#update_client", function(e) {
        e.preventDefault();
        $.ajax({
            url: '/clients/'+$('#edit_client_id').val(),
            method:'PUT',
            dataType: 'json',
            data:$('#edit_form').serialize(),
            success: function (result) {
                $('#edit_client_modal').modal('toggle');
                $('#successMessage').show();
                $('#message').html(result.message);
                table.ajax.reload();
            },
        });
    });

});